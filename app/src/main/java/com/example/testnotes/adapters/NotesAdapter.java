package com.example.testnotes.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.testnotes.R;
import com.example.testnotes.models.Note;

import java.util.List;

public class NotesAdapter extends ArrayAdapter<Note> {
    private final List<Note> elements;
    private LayoutInflater inflater;
    private int layout;
    private final Context mContext;

    public NotesAdapter(Context context, int resource, List<Note> elements) {
        super(context, resource, elements);
        this.elements = elements;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
        this.mContext = context;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final NotesAdapter.ViewHolder viewHolder;
        final Note note = elements.get(i);

        if (convertView == null) {
            convertView = inflater.inflate(this.layout, parent, false);
            viewHolder = new NotesAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (NotesAdapter.ViewHolder) convertView.getTag();
        }

        viewHolder.nameView.setText(note.getName());
        viewHolder.descView.setText(note.getDescription());
        return convertView;
    }

    private class ViewHolder {
        final TextView nameView;
        final TextView descView;
        ViewHolder(View view){
            nameView = view.findViewById(R.id.note_name);
            descView = view.findViewById(R.id.note_desc);
        }
    }
}
