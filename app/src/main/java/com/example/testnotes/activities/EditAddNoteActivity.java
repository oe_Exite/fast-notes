package com.example.testnotes.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testnotes.R;
import com.example.testnotes.bll.interfaces.INotesService;
import com.example.testnotes.bll.services.NotesService;
import com.example.testnotes.models.Note;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import static com.example.testnotes.activities.MainActivity.NOTE_INDEX;
import static com.example.testnotes.activities.MainActivity.NOTE_OBJ;
import static com.example.testnotes.activities.MainActivity.NOTE_RES;
import static com.example.testnotes.activities.MainActivity.RESULT_REMOVE;


@EActivity(R.layout.activity_editadd)
public class EditAddNoteActivity extends AppCompatActivity {

    @ViewById(R.id.note_name_text)
    EditText noteName;
    @ViewById(R.id.note_description_text)
    EditText noteDescription;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.remove_btn)
    Button removeBtn;

    @Bean(NotesService.class)
    INotesService notesService;

    Note noteToEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Gson gson = new Gson();
        noteToEdit = gson.fromJson(getIntent().getStringExtra(NOTE_OBJ), Note.class);
    }

    @AfterViews
    protected void afterViews() {
        Log.i("EditAddNoteActivity", "afterViews");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (noteToEdit != null) {
            noteName.setText(noteToEdit.getName());
            noteDescription.setText(noteToEdit.getDescription());
            removeBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Click(R.id.save_btn)
    public void SaveClick() {
        String name = noteName.getText().toString();
        String desc = noteDescription.getText().toString();
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(desc)) {
            Toast.makeText(getApplicationContext(), "Note name and description are required", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent();
        Gson gson = new Gson();
        Note newNote;
        if (noteToEdit != null) {
            noteToEdit.setName(name);
            noteToEdit.setDescription(desc);
            newNote = notesService.editNote(noteToEdit);
            int noteIndex = getIntent().getIntExtra(NOTE_INDEX, -1);
            intent.putExtra(NOTE_INDEX, noteIndex);
        } else {
            newNote = notesService.addNote(name, desc);
        }
        if (newNote != null) {
            intent.putExtra(NOTE_RES, gson.toJson(newNote));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Click(R.id.remove_btn)
    public void RemoveClick() {
        if (noteToEdit != null) {
            boolean isSuccess = notesService.removeNote(noteToEdit);
            if (isSuccess) {
                Intent intent = new Intent();
                int noteIndex = getIntent().getIntExtra(NOTE_INDEX, -1);
                intent.putExtra(NOTE_INDEX, noteIndex);
                setResult(RESULT_REMOVE, intent);
                finish();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("EditAddNoteActivity", "onStart");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("EditAddNoteActivity", "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("EditAddNoteActivity", "onDestroy");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("EditAddNoteActivity", "onStop");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("EditAddNoteActivity", "onPause");
    }
}
