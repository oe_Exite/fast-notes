package com.example.testnotes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.testnotes.R;
import com.example.testnotes.adapters.NotesAdapter;
import com.example.testnotes.bll.interfaces.INotesService;
import com.example.testnotes.bll.services.NotesService;
import com.example.testnotes.models.Note;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    public static final String NOTE_OBJ = "note_obj";
    public static final String NOTE_INDEX = "note_index";
    public static final String NOTE_RES = "note_res";

    public static final int RESULT_REMOVE = RESULT_FIRST_USER + 1;

    private static final int REQUEST_ADD = 1;
    private static final int REQUEST_EDIT = 2;

    @ViewById(R.id.notes_list)
    ListView notesListView;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.empty_list_txt)
    TextView emptyListTxt;

    @Bean(NotesService.class)
    INotesService notesService;

    List<Note> notes;
    NotesAdapter notesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        Log.i("MainActivity", "onCreate");
        notesService.setContext(getApplicationContext());
    }

    @AfterViews
    void afterViews() {
        Log.i("MainActivity", "afterViews");
        setSupportActionBar(toolbar);
        notes = notesService.getNotes();
        notesAdapter = new NotesAdapter(this, R.layout.item_note, notes);
        notesListView.setAdapter(notesAdapter);
        if (notes == null || notes.isEmpty()) {
            emptyListTxt.setVisibility(View.VISIBLE);
        }
    }

    @ItemClick(R.id.notes_list)
    void chooseNote(int noteIndex) {
        Note note = notes.get(noteIndex);
        Log.i("chosen note", note.getName());
        Gson gson = new Gson();
        String jsonNote = gson.toJson(note);
        Intent intent = new Intent(this, EditAddNoteActivity_.class);
        intent.putExtra(NOTE_OBJ, jsonNote);
        intent.putExtra(NOTE_INDEX, noteIndex);
        startActivityForResult(intent, REQUEST_EDIT);
    }

    @Click(R.id.fab_add)
    void addClick() {
        Intent intent = new Intent(this, EditAddNoteActivity_.class);
        startActivityForResult(intent, REQUEST_ADD);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("MainActivity", "onStart");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ADD:
                if (resultCode == RESULT_OK) {
                    if (emptyListTxt.getVisibility() == View.VISIBLE) {
                        emptyListTxt.setVisibility(View.GONE);
                    }
                    Gson gson = new Gson();
                    Note newNote = gson.fromJson(data.getStringExtra(NOTE_RES), Note.class);
                    notes.add(newNote);
                    notesAdapter.notifyDataSetChanged();
                }
                break;
            case REQUEST_EDIT:
                if (resultCode == RESULT_OK) {
                    Gson gson = new Gson();
                    Note editedNote = gson.fromJson(data.getStringExtra(NOTE_RES), Note.class);
                    int noteIndex = data.getIntExtra(NOTE_INDEX, -1);
                    if (noteIndex != -1) {
                        notes.set(noteIndex, editedNote);
                        notesAdapter.notifyDataSetChanged();
                    }
                } else if (resultCode == RESULT_REMOVE) {
                    int noteIndex = data.getIntExtra(NOTE_INDEX, -1);
                    if (noteIndex != -1) {
                        notes.remove(noteIndex);
                        notesAdapter.notifyDataSetChanged();
                        if (notes.isEmpty()) {
                            emptyListTxt.setVisibility(View.VISIBLE);
                        }
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("MainActivity", "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("MainActivity", "onDestroy");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("MainActivity", "onStop");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("MainActivity", "onPause");
    }
}
