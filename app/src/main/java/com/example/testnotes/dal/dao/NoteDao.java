package com.example.testnotes.dal.dao;

import com.example.testnotes.models.Note;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class NoteDao extends BaseDaoImpl<Note, Integer> {

    public NoteDao(Class<Note> dataClass) throws SQLException {
        super(dataClass);
    }

    public NoteDao(ConnectionSource connectionSource, Class<Note> gmPlaceClass) throws SQLException {
        super(connectionSource,gmPlaceClass);
    }
}
