package com.example.testnotes.dal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.testnotes.dal.dao.NoteDao;
import com.example.testnotes.models.Note;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.EBean;

import java.sql.SQLException;

@EBean(scope = EBean.Scope.Singleton)
public class DbProvider extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DbProvider.class.getSimpleName();


    private static final String DATABASE_NAME ="testnotes.db";

    private static final int DATABASE_VERSION = 1;

    private NoteDao noteDao = null;

    public DbProvider(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try
        {
            TableUtils.createTable(connectionSource, Note.class);
        }
        catch (SQLException e){
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer) {
        try {
            TableUtils.dropTable(connectionSource, Note.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e) {
            Log.e(TAG,"error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }


    public NoteDao getNoteDao() throws SQLException {
        if (noteDao == null) {
            noteDao = new NoteDao(getConnectionSource(), Note.class);
        }
        return noteDao;
    }

    @Override
    public void close() {
        super.close();
        noteDao = null;
    }
}
