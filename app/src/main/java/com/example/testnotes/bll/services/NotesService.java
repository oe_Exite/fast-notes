package com.example.testnotes.bll.services;

import android.content.Context;
import android.widget.Toast;

import com.example.testnotes.dal.DbProvider;
import com.example.testnotes.dal.dao.NoteDao;
import com.example.testnotes.models.Note;
import com.example.testnotes.bll.interfaces.INotesService;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.ormlite.annotations.OrmLiteDao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@EBean(scope = EBean.Scope.Singleton)
public class NotesService implements INotesService {

    @Bean
    DbProvider provider;

    @OrmLiteDao(helper = DbProvider.class)
    NoteDao noteDao;

    private Context context;

    @AfterInject
    void initDao() {
        try {
            if (noteDao == null && provider != null) {
                noteDao = provider.getNoteDao();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean checkIfNoteNameExists(String name) throws SQLException {
        Note note = noteDao.queryBuilder().where().eq("name", name).queryForFirst();
        if (note != null) {
            Toast.makeText(context, "Note with this name already exists", Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public List<Note> getNotes() {
        try {
            return noteDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Note addNote(String name, String description) {
        try {
            if (!checkIfNoteNameExists(name)) {
                Note note = new Note(name, description);
                note.setAddDate(new Date());
                noteDao.create(note);
                Toast.makeText(context, "Note was added", Toast.LENGTH_LONG).show();
                return note;
            }
        } catch (SQLException e) {
            Toast.makeText(context, "Error occurred", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Note editNote(Note note) {
        try {
            if (!checkIfNoteNameExists(note.getName())) {
                note.setEditDate(new Date());
                noteDao.update(note);
                Toast.makeText(context, "Note was edited", Toast.LENGTH_LONG).show();
                return note;
            }
        } catch (SQLException e) {
            Toast.makeText(context, "Error occurred", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean removeNote(Note note) {
        try {
            noteDao.delete(note);
            Toast.makeText(context, "Note was removed", Toast.LENGTH_LONG).show();
            return true;
        } catch (SQLException e) {
            Toast.makeText(context, "Error occurred", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return false;
    }

}
