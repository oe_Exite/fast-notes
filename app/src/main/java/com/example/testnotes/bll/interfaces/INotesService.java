package com.example.testnotes.bll.interfaces;

import android.content.Context;

import com.example.testnotes.models.Note;

import java.util.List;

public interface INotesService {
    Note addNote(String name, String description);
    Note editNote(Note note);
    boolean removeNote(Note note);
    List<Note> getNotes();
    void setContext(Context context);
}
